import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationComponent } from './application/application.component';
import { IntroRoutingModule } from './intro-routing.module';
import { ModulesComponent } from './modules/modules.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ActionsComponent } from './actions/actions.component';



@NgModule({
  declarations: [
    ApplicationComponent,
    ModulesComponent,
    ActionsComponent,
    
  ],
  imports: [
    CommonModule,
    IntroRoutingModule,
    ReactiveFormsModule
  ],
  providers:[
    
  ]
})
export class IntroModule { }
