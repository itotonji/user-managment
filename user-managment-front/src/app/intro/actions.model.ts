export class ActionsModel{
  public id:number=0;
  public code:string='';
  public name:string='';
  public title:string='';
  public titleAr:string='';
  public isActive:boolean=false;
  public isView:boolean=false;
  public groupid:number=1;
  public moduleid:number=1;
}



