export class ModulesModel{
  public id:number=0;
  public code:string='';
  public name:string='';
  public title:string='';
  public titleAr:string='';
  public isActive:boolean=false;
  public applicationid:number=1;
}
