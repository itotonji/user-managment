import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/shared/apiِِApplications.service';
import {ActionsModel} from "../actions.model";

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {
  actionsForm!:FormGroup;
  actionData:any;
  modelObj: ActionsModel = new ActionsModel();

  constructor(private formBuilder:FormBuilder,private api :ApiService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.actionsForm=this.formBuilder.group({
      'code':[''],
      'name':[''],
      'title':[''],
      'titleAr':[''],

      'isActive':[''],
      'isView':[''],
      'groupid':[''],
      'moduleid':['']
    });
    this.getAllActions();
  }
  getAllActions(){
    this.api.getAction()
    .subscribe(res=>{
      this.actionData = res;
    })
  }

  onEditAction(row:any){

    this.modelObj.id = row.id;
    this.actionsForm.controls['code'].setValue(row.code);
    this.actionsForm.controls['name'].setValue(row.name);
    this.actionsForm.controls['title'].setValue(row.title);
    this.actionsForm.controls['titleAr'].setValue(row.titleAr);
    this.actionsForm.controls['isActive'].setValue(row.isActive);
    this.actionsForm.controls['isView'].setValue(row.isView);
    this.actionsForm.controls['groupid'].setValue(row.groupid);
    this.actionsForm.controls['moduleid'].setValue(row.moduleid);

    console.log(row);
  }
  // deleteAction(row:any){
  //   this.api.deleteAction(row.id)
  //     .subscribe(res=>{
  //       alert('Action deleted');
  //       this.getAllActions();
  //     })
  // }


  clickAddAction(){
    this.actionsForm.reset();

  }
  updateActionsDetails(){
    this.modelObj.code = this.actionsForm.value.code;
    this.modelObj.name = this.actionsForm.value.name;
    this.modelObj.title = this.actionsForm.value.title;
    this.modelObj.titleAr = this.actionsForm.value.titleAr;


    this.modelObj.isActive = this.actionsForm.value.isActive;
    this.modelObj.isView = this.actionsForm.value.isView;
    this.modelObj.groupid = this.actionsForm.value.groupid;
    this.modelObj.moduleid = this.actionsForm.value.moduleid;

    this.api.updateAction(this.modelObj,this.modelObj.id)
      .subscribe(res=>{
        alert('updated');
        let ref = document.getElementById('cancel')
        ref?.click();
        this.actionsForm.reset();
        this.getAllActions();
      })
  }
  postActionsDetails(){
    this.modelObj.code = this.actionsForm.value.code;
    this.modelObj.name = this.actionsForm.value.name;
    this.modelObj.title = this.actionsForm.value.title;
    this.modelObj.titleAr = this.actionsForm.value.titleAr;
    this.modelObj.isActive = this.actionsForm.value.isActive;
    this.modelObj.isView = this.actionsForm.value.isView;
    this.modelObj.groupid = this.actionsForm.value.groupid;
    this.modelObj.moduleid = this.actionsForm.value.moduleid;


    this.api.postAction(this.modelObj)
      .subscribe(res=>{
        console.log(res);
        alert('Action added sucessfully')
        let ref = document.getElementById('cancel')
        ref?.click();
        this.actionsForm.reset();
        this.getAllActions();
      })
  }

}
