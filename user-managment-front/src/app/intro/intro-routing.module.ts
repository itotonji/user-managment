import {NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActionsComponent } from './actions/actions.component';
import { ApplicationComponent } from './application/application.component';
import { ModulesComponent } from './modules/modules.component';

const routes: Routes = [
    {path:'application', component:ApplicationComponent},
    {path:'application/:id',component:ModulesComponent},
    {path:'application/:moduleId/:actionId', component:ActionsComponent},
    {path:'application/actions', component:ActionsComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class IntroRoutingModule { }
