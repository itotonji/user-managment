import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup} from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';
import { ApiService } from 'src/app/shared/apiِِApplications.service';
import { ApplicationModel } from '../application.model';


@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  moduleId:any;
  applicationForm!:FormGroup;
  applicationData!:any;
  showAdd !: boolean;
  showUpdate !: boolean;
  applicationModelObj: ApplicationModel = new ApplicationModel();
  constructor(private router:Router, private formBuilder:FormBuilder,private api :ApiService ,private route:ActivatedRoute) {}
  ngOnInit(): void {
    this.applicationForm=this.formBuilder.group({
      'code':[''],
      'name':[''],
      'title':[''],
      'titleAr':[''],
      'isActive':[''],


    });
    this.getAllApplications();

  }

  getAllApplications(){
    this.api.getApplication()
    .subscribe(res=>{
      this.applicationData = res;
    })
  }

  onEdit(row:any){
    this.showAdd = false;
    this.showUpdate = true;
    this.applicationModelObj.id = row.id;
    this.applicationForm.controls['code'].setValue(row.code);
    this.applicationForm.controls['name'].setValue(row.name);
    this.applicationForm.controls['title'].setValue(row.title);
    this.applicationForm.controls['titleAr'].setValue(row.titleAr);
    this.applicationForm.controls['isActive'].setValue(row.isActive);

    console.log(row);
  }

  deleteApplication(row:any){
    this.api.deleteApplication(row.id)
      .subscribe(res=>{
        alert('Application deleted');
        this.getAllApplications();
      })
  }
  clickAddApplication(){
    this.applicationForm.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }

  updateApplicationDetails(){
    this.applicationModelObj.code = this.applicationForm.value.code;
    this.applicationModelObj.name = this.applicationForm.value.name;
    this.applicationModelObj.title = this.applicationForm.value.title;
    this.applicationModelObj.titleAr = this.applicationForm.value.titleAr;
    this.applicationModelObj.isActive = this.applicationForm.value.isActive;

    this.api.updateApplication(this.applicationModelObj)
      .subscribe(res=>{
        alert('updated');
        let ref = document.getElementById('cancel')
        ref?.click();
        this.applicationForm.reset();
        this.getAllApplications();
      })
  }





  postApplicationDetails(){
    this.applicationModelObj.code = this.applicationForm.value.code;
    this.applicationModelObj.name = this.applicationForm.value.name;
    this.applicationModelObj.title = this.applicationForm.value.title;
    this.applicationModelObj.titleAr = this.applicationForm.value.titleAr;
    this.applicationModelObj.isActive = this.applicationForm.value.isActive;



    this.api.postApplication(this.applicationModelObj)
      .subscribe(res=>{
        console.log(res);
        alert('Application added sucessfully')
        let ref = document.getElementById('cancel')
        ref?.click();
        this.applicationForm.reset();
        this.getAllApplications();
      })
  }

  getApplicationById(data:any){
    this.api.getApplicationById(this.applicationModelObj.id)
      .subscribe(res=>{
        this.applicationData = res;
      })
    this.api.navigate(data);
  }
  // showModulesComponent(data:any){
  //   this.api.navigate(data);
  // }
}
