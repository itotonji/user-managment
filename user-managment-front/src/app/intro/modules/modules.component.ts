import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/shared/apiِِApplications.service';
import { ModulesModel } from '../modules.model';


@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {
showListOfActions=false;
modulesForm!:FormGroup;
actionId:any;
moduleData:any;
modelObj: ModulesModel = new ModulesModel();


  constructor(private router:Router, private formBuilder:FormBuilder,private api :ApiService,private route:ActivatedRoute ) {}

  ngOnInit(): void {
    this.modulesForm=this.formBuilder.group({
      'code':[''],
      'name':[''],
      'title':[''],
      'titleAr':[''],
      'isActive':[''],
      'applicationid':['']

    });

    this.getAllModules();


  }

  getAllModules(){
    this.api.getModule()
    .subscribe(res=>{
      this.moduleData = res;
    })
  }




  onEditModule(row:any){

    this.modelObj.id = row.id;
    this.modulesForm.controls['code'].setValue(row.code);
    this.modulesForm.controls['name'].setValue(row.name);
    this.modulesForm.controls['title'].setValue(row.title);

    this.modulesForm.controls['titleAr'].setValue(row.titleAr);

    this.modulesForm.controls['isActive'].setValue(row.isActive);
    this.modulesForm.controls['applicationid'].setValue(row.applicationid);




    console.log(row);
  }
  deleteModule(row:any){
    this.api.deleteModule(row.id)
      .subscribe(res=>{
        alert('Module deleted');
        this.getAllModules();
      })
  }

  clickAddModule(){
    this.modulesForm.reset();

  }





  updateModulesDetails(){
    this.modelObj.code = this.modulesForm.value.code;
    this.modelObj.name = this.modulesForm.value.name;
    this.modelObj.title = this.modulesForm.value.title;
    this.modelObj.titleAr = this.modulesForm.value.titleAr;
    this.modelObj.isActive = this.modulesForm.value.isActive;
    this.modelObj.applicationid = this.modulesForm.value.applicationid;


    this.api.updateModule(this.modelObj)
      .subscribe(res=>{
        alert('updated');
        let ref = document.getElementById('cancel')
        ref?.click();
        this.modulesForm.reset();
        this.getAllModules();
      })
  }



  postModulesDetails(){
    this.modelObj.code = this.modulesForm.value.code;
    this.modelObj.name = this.modulesForm.value.name;
    this.modelObj.title = this.modulesForm.value.title;

    this.modelObj.titleAr = this.modulesForm.value.titleAr;

    this.modelObj.isActive = this.modulesForm.value.isActive;

    this.modelObj.applicationid = this.modulesForm.value.applicationid;




    this.api.postModule(this.modelObj)
      .subscribe(res=>{
        console.log(res);
        alert('Module added sucessfully')
        let ref = document.getElementById('cancel')
        ref?.click();
        this.modulesForm.reset();
        this.getAllModules();
      })
  }


getModuleById(data:any){
  this.api.getModuleById(this.modelObj.id)
    .subscribe(res=>{
      this.moduleData = res;
    })
  this.actionId=data.id;
  this.router.navigate(['/application',this.api.moduleId,data.id]);
  console.log(data.id);
}
  // showActionsComponent(data:any){
  //   this.actionId=data.id;
  //   this.router.navigate(['/application',this.api.moduleId,data.id]);
  //   console.log(data.id);
  //
  // }

}
