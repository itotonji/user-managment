import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {map} from "rxjs";
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiUrl="http://192.168.1.136:8070/api/";

  moduleId:any;

  constructor(private http: HttpClient,
    private router:Router) { }

    //application functions
  postApplication(data:any){
    return this.http.post<any>(this.apiUrl+'application',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getApplication(){
    const headers = { 'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Authorization' : 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJvbWFyIiwiZXhwIjoxNjY3MjE0ODUwLCJpZCI6MiwiaWF0IjoxNjY3MTI4NDUwfQ.VuCeD618dvYsKPgjlO-b_vvyQJYE2J-ChLI9fmqXvMFAbgQOZALph9pBOeoIl5mCIjB7tBw0KW38EtH5AHgwiMPB-5rZiamjHQ2ynMhJmzsGkXtclmfEB7Uu7k8KvLzfIiQfHEQ2-6Xb6IFYP-9qcARIlXeLRYYMsQtN442etYE'
    }
    return this.http.get<any>(this.apiUrl+'application', {headers})
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getApplicationById(id:number){
    return this.http.get<any>(this.apiUrl+'application/'+id)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  updateApplication(data:any){
    return this.http.put<any>(this.apiUrl+'application',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }

  deleteApplication(id:number){
    return this.http.delete<any>(this.apiUrl+'application/'+id)
      .pipe(map((res:any)=>{
        return res;
      }))
  }



//module function

  postModule(data:any){
    return this.http.post<any>(this.apiUrl+'module',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getModule(){
    return this.http.get<any>(this.apiUrl+'application')
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getModuleById(id:number){
    return this.http.get<any>(this.apiUrl+'module/'+id)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  updateModule(data:any){
    return this.http.put<any>(this.apiUrl+'module',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  deleteModule(id:number){
    return this.http.delete<any>(this.apiUrl+'module/'+id)
      .pipe(map((res:any)=>{
        return res;
      }))
  }









//action functions


  postAction(data:any){
    return this.http.post<any>(this.apiUrl+'action',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getAction(){
    return this.http.get<any>(this.apiUrl+'action')
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  updateAction(data:any,id:number){
    return this.http.put<any>(this.apiUrl+'action/'+id,data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  deleteAction(id:number){
    return this.http.delete<any>(this.apiUrl+'action/'+id)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  //query params function

  navigate(data:any){
    this.moduleId=data.id;
    this.router.navigate(['/application',data.id])

  console.log(data.id);

  }

}

