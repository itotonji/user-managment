import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {map, retry} from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiUrl="http://192.168.1.136:8070/api/";

  constructor(private http: HttpClient) { }

  getAllUsers(){
    return this.http.get<any>(this.apiUrl)
      .pipe(map((res:any)=>{
        return res;
      }))
  }

  getRoleUsers(){
    return this.http.get<any>(this.apiUrl+'roleUser')
      .pipe(map((res:any)=>{
        return res;
      }))
  }

  getAllActions(){
    return this.http.get<any>(this.apiUrl+'actions')
      .pipe(map((res:any)=>{
        return res;
      }))
  }

  addRoleUser(data:any){
    return this.http.post<any>(this.apiUrl+'roleUser',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  removeRoleUser(id:any){
    return this.http.delete<any>(this.apiUrl+'roleUser/'+id)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  addRoleAction(data:any){
    return this.http.post<any>(this.apiUrl+'roleActions',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }

  postRole(data:any){
    return this.http.post<any>(this.apiUrl+'role',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getRole(){
    return this.http.get<any>(this.apiUrl+'role')
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  updateRole(data:any,id:number){
    return this.http.put<any>(this.apiUrl+'role/'+id,data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  deleteRole(id:number){
    return this.http.delete<any>(this.apiUrl+'role/'+id)
      .pipe(map((res:any)=>{
        return res;
      }))
  }


  postFacility(data:any){
    return this.http.post<any>(this.apiUrl+'facility',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getFacility(){
    return this.http.get<any>(this.apiUrl+'facility')
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  updateFacility(data:any,id:number){
    return this.http.put<any>(this.apiUrl+'facility/'+id,data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  // deleteFacility(id:number){
  //   return this.http.delete<any>(this.apiUrl+'facility/'+id)
  //     .pipe(map((res:any)=>{
  //       return res;
  //     }))
  // }

  getFacilityUsers(){
    return this.http.get<any>(this.apiUrl+'facilityUser')
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  postGroup(data:any){
    return this.http.post<any>(this.apiUrl+'group',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  getGroup(){
    return this.http.get<any>(this.apiUrl+'group')
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  updateGroup(data:any,id:number){
    return this.http.put<any>(this.apiUrl+'group'+id,data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }

  addGroupAction(data:any){
    return this.http.post<any>(this.apiUrl+'groupActions',data)
      .pipe(map((res:any)=>{
        return res;
      }))
  }

  getGroupActions(){
    return this.http.get<any>(this.apiUrl+'groupActions')
      .pipe(map((res:any)=>{
        return res;
      }))
  }

}

