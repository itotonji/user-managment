import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup} from "@angular/forms";
import {user} from '../user';
import {UserService} from "../user.service";
// import {ApiService} from "../shared/api.service";


@Component({
  selector: 'app-user',
  templateUrl: './user-component.component.html',
  styleUrls: ['./user-component.component.css']
})
export class UserComponent implements OnInit {

  profileForm !: FormGroup;
  userModelObj: user = new user();
  userData !: any
  showAdd !: boolean;
  showUpdate !: boolean;
  showChanged!:boolean
  passwordform !: FormGroup;
  constructor(private formBuilder:FormBuilder,
              private api:UserService) {

  }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      firstName:[''],
      lastName:[''],
      userName:[''],
      password:[''],
      // clientID:[]

    })
    console.log(this.userData)
    this.getAllUsers();
    this.passwordform=this.formBuilder.group({
      clientID:[],
      oldPassword:[],
      newPassword:[]
    })
  }

  postUserDetails(){
    this.userModelObj.firstName = this.profileForm.value.firstName;
    this.userModelObj.lastName = this.profileForm.value.lastName;
    this.userModelObj.userName = this.profileForm.value.userName;
    this.userModelObj.password = this.profileForm.value.password;
    this.api.postUser(this.userModelObj)
      .subscribe(res=>{
          console.log(res);
          alert('user added successfully')
          let ref = document.getElementById('cancel')
          ref?.click();
          this.profileForm.reset();
          this.getAllUsers();
        })
  }

  getAllUsers(){
    this.api.getUsers()
      .subscribe(res=>{
        this.userData = res.data;
      })
  }
  // onDelete (row:any)
  // {
  //   this.api.deletuser(row.id).subscribe(res=>{
  //     alert('user deleted');
  //     this.getAllUsers();
  //   })
  //
  // }
  onEdit(row:any){
    this.showAdd = false;
    this.showUpdate = true;
    this.userModelObj.clientID= row.id;
    this.profileForm.controls['firstName'].setValue(row.firstName);
    this.profileForm.controls['lastName'].setValue(row.lastName);
    this.profileForm.controls['userName'].setValue(row.userName);
  }
  clickAddUser(){
    this.profileForm.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }

  updateUserDetails(){
    this.userModelObj.firstName = this.profileForm.value.firstName;
    this.userModelObj.lastName = this.profileForm.value.lastName;
    this.userModelObj.userName = this.profileForm.value.userName;
    // this.userModelObj.clientID = this.profileForm.value.clientID;

    // need not to change client id and password
    console.log( 'onupdate',this.userModelObj)
    this.api.updateUser(this.userModelObj,this.userModelObj.clientID)
      .subscribe(res=>{
        alert('updated');
        let ref = document.getElementById('cancel')
        ref?.click();
        this.profileForm.reset();
        this.getAllUsers();
      })
  }

  deActivateUser(row:any)
  {
    if (this.userModelObj.isActive===true)
    {
      this.userModelObj.isActive =false;
    }
    else
      this.userModelObj.isActive=true;
    this.api.deActiveUser( this.userModelObj,row.id)
      .subscribe(res=>{
        alert('user deActivated');
        this.getAllUsers();
      })
  }
  onchangePassword(row :any)
  {
    this.userModelObj.clientID= row.id;
    this.passwordform.controls['oldPassword'].setValue(row.password);
  }
  changePassword(){
    this.userModelObj.password=this.passwordform.value.newPassword
    console.log( 'on change password ',this.userModelObj)
    this.api.changePassword(this.userModelObj,this.userModelObj.clientID)
      .subscribe(res=>{

        let ref = document.getElementById('cancelChange')
        ref?.click();
        this.getAllUsers();
      })
  }
}
