import {Injectable} from '@angular/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {map} from "rxjs";
import {ApiResponse} from "./user";
@Injectable(
  {
    providedIn :'root'
  }
)
export  class UserService{

  apiUrl="http://192.168.1.136:8070/api/";
  constructor(private http: HttpClient) {
  }
  postUser(data: any)
  {
    return this.http.post(this.apiUrl+'user',data).pipe(map(res=>{
      return res;
    }))
  }

  updateUser(data :any, id:number)
  {
    return this.http.put(this.apiUrl+'user',data).pipe(map(res=>{
      return res;
    }))
  }
  getUsers()
  {
    const headers = { 'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Authorization' : 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJvbWFyIiwiZXhwIjoxNjY3MjE0ODUwLCJpZCI6MiwiaWF0IjoxNjY3MTI4NDUwfQ.VuCeD618dvYsKPgjlO-b_vvyQJYE2J-ChLI9fmqXvMFAbgQOZALph9pBOeoIl5mCIjB7tBw0KW38EtH5AHgwiMPB-5rZiamjHQ2ynMhJmzsGkXtclmfEB7Uu7k8KvLzfIiQfHEQ2-6Xb6IFYP-9qcARIlXeLRYYMsQtN442etYE'
    }
   return this.http.get<ApiResponse>(this.apiUrl+'user', {headers}).pipe(map(res=>{
     return res;
   }))
  }
  deActiveUser(data:any,id:number)
  {
    return this.http.put(this.apiUrl+'user/activation',data).pipe(map(res=>{
      return res;
    }))
  }
  // deletuser(id:number)
  // {
  //   return this.http.delete(this.url+'/'+id).
  //   pipe(map(res=>{
  //     return res;
  //   }))
  // }
  changePassword(data:any,id:number)
  {
    return this.http.put(this.apiUrl+'user/resetPassword',data).pipe(map(res=>{
      return res;
    }))
  }
}

