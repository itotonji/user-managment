import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { UserComponentComponent } from './user-component/user-component.component';
import {RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {UserComponent} from "./user-component/userComponent";
import {LogInComponent} from "../log-in/log-in.component";
import {NavComponent} from "../nav/nav.component";


const routes :Routes=[
 // {path:'user',component:UserComponentComponent},
  {path:'user',component:UserComponent}
]
@NgModule({
  declarations: [
    //UserComponentComponent,
    UserComponent,LogInComponent,NavComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    ReactiveFormsModule,
  ],
  exports:[
    //UserComponentComponent
    RouterModule,UserComponent,LogInComponent,NavComponent]


})
export class UserModule { }
