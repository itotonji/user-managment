import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacilitiesComponent } from './facilities.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";
import {GroupsComponent} from "../groups/groups.component";


const routes :Routes=[
  // {path:'user',component:UserComponentComponent},
  {path:'facilities',component:FacilitiesComponent}
]

@NgModule({
  declarations: [
    FacilitiesComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    ReactiveFormsModule

  ]
})
export class FacilitiesModule { }
