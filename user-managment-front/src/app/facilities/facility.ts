export class facility{
  facilityId: number= 0;
  facilityName: string= '';
  facilityNameAr: string= '';
  facilityLicense: string= '';
  facilityActive: boolean= true;
}
