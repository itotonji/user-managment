import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {facility} from "./facility";
import {ApiService} from "../shared/api.service";
import {FacilityUser} from "./facilityUser";

@Component({
  selector: 'app-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.css']
})
export class FacilitiesComponent implements OnInit {

  facilityList!:FormGroup;
  addUsers!:FormGroup;
  edit!:FormGroup;
  facilityUserObj: FacilityUser = new FacilityUser();
  facilityData!:any;
  facilityUsersData!:any;
  facilityUsersList!:any;
  currentFacilityId!:any;
  facilityObj:facility = new facility();
  showAdd!:boolean;
  showUpdate!:boolean;

  constructor(private fb:FormBuilder,
              private api:ApiService) {


  }

  ngOnInit(): void {
    this.facilityList=this.fb.group({
      // 'id':[null],
      'facilityName':[null],
      'facilityNameAr':[null],
      'facilityLicense':[null],
      'facilityActive':[null]
    })

    this.addUsers=this.fb.group({
      'users':[null]
    })
    this.edit=this.fb.group({
      'code':[null],
      'name':[null],
      'arabicName':[null],
      'users':[null]
    })
    this.getAllFacilities();

  }


  facilityActivation(row:any){
    row.facilityActive =! row.facilityActive;
    console.log(row.facilityActive)
    // this.showAdd = false;
    // this.showUpdate = true;
    this.facilityObj.facilityId = row.id;
    // this.rolesList.controls['roleId'].setValue(row.id);
    this.facilityList.controls['facilityName'].setValue(row.facilityName);
    this.facilityList.controls['facilityNameAr'].setValue(row.facilityNameAr);
    this.facilityList.controls['facilityLicense'].setValue(row.facilityLicense);
    this.facilityList.controls['facilityActive'].setValue(row.facilityActive);
    this.updateFacilityDetails();
  }

  getAllFacilities(){
    this.api.getFacility()
      .subscribe(res=>{
        this.facilityData = res;
      })
  }

  getAllUsers(row:any){
    this.currentFacilityId = row.id;
    this.api.getAllUsers()
      .subscribe((res:any)=>{
        this.facilityUsersData = res;
      })
  }

  getUsers(){
    this.api.getAllUsers()
      .subscribe((res:any)=>{
        this.facilityUsersData = res;
      })
  }

  addFacilityUser(row:any){
    this.facilityUserObj.userId = row.id;
    this.facilityUserObj.facilityId = this.currentFacilityId;
    this.api.addRoleUser(this.facilityUserObj)
      .subscribe(res=>{
        alert('FacilityUser Added');
      })
    this.getUsers();
  }

  viewFacilityUsers(){
    this.api.getFacilityUsers()
      .subscribe(res=>{
        this.facilityUsersList = res;
      })
  }


  clickAddFacility(){
    this.facilityList.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }

  postFacilityDetails(){
    this.facilityObj.facilityName = this.facilityList.value.facilityName;
    this.facilityObj.facilityNameAr = this.facilityList.value.facilityNameAr;
    this.facilityObj.facilityLicense = this.facilityList.value.facilityLicense;
    this.facilityObj.facilityActive = this.facilityList.value.facilityActive;

    this.api.postFacility(this.facilityObj)
      .subscribe(res=>{
          console.log(res);
          alert('user added sucessfully')
          let ref = document.getElementById('cancel')
          ref?.click();
          this.facilityList.reset();
          this.getAllFacilities();
        },
        err=>{
          alert(err)
        })
  }


  onEdit(row:any){
    this.showAdd = false;
    this.showUpdate = true;
    this.facilityObj.facilityId = row.id;
    this.facilityList.controls['facilityName'].setValue(row.facilityName);
    this.facilityList.controls['facilityNameAr'].setValue(row.facilityNameAr);
    this.facilityList.controls['facilityLicense'].setValue(row.facilityLicense);
    // this.rolesList.controls['isActive'].setValue(row.isActive);
  }

  updateFacilityDetails(){
    this.facilityObj.facilityName = this.facilityList.value.facilityName;
    this.facilityObj.facilityNameAr = this.facilityList.value.facilityNameAr;
    this.facilityObj.facilityLicense = this.facilityList.value.facilityLicense;
    this.facilityObj.facilityActive = this.facilityList.value.facilityActive;
    this.api.updateFacility(this.facilityObj,this.facilityObj.facilityId)
      .subscribe(res=>{
        alert('updated');
        let ref = document.getElementById('facilityCancel')
        ref?.click();
        this.facilityList.reset();
        this.getAllFacilities();
      })
  }

}
