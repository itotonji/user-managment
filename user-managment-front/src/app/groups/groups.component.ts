import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ApiService} from "../shared/api.service";
import {GroupActions} from "./groupActions";
import {Group} from "./group";

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {

  groupList!:FormGroup;
  groupObj: Group = new Group();
  groupActionObj: GroupActions = new GroupActions();
  groupsData!: any;
  groupActionsList!: any;
  groupActionsData!: any;
  currentGroupId!: any;
  showAdd!:boolean;
  showUpdate!:boolean;
  // actions:FormGroup;

  constructor(private fb:FormBuilder,
              private api:ApiService) {

      // this.actions=fb.group({
      //   'actionName':[null],
      // })
    }

  ngOnInit(): void {
    this.groupList=this.fb.group({
      'code' :[null],
      'groupId':[null],
      'groupName':[null],
      'grouptitle':[null],
      'grouptitleAr':[null],
      'groupActive':[null],
    })

    this.getAllGroups();
  }

  getAllGroups(){
    this.api.getGroup()
      .subscribe(res=>{
        this.groupsData = res;
      })
  }

  clickAddGroup(){
    this.groupList.reset();
    this.showAdd = true;
    // this.showUpdate = false;
  }


  postGroupDetails(){
    this.groupObj.code = this.groupList.value.code;
    this.groupObj.groupId = this.groupList.value.groupId;
    this.groupObj.groupName = this.groupList.value.groupName;
    this.groupObj.grouptitle = this.groupList.value.grouptitle;
    this.groupObj.grouptitleAr = this.groupList.value.grouptitleAr;
    this.groupObj.groupActive = false;

    this.api.postGroup(this.groupObj)
      .subscribe(res=>{
          console.log(res);
          alert('user added sucessfully')
          let ref = document.getElementById('cancel')
          ref?.click();
          this.groupList.reset();
          this.getAllGroups();
        },
        err=>{
          alert(err)
        })
  }

  groupActivation(row:any){
    row.isActive = !row.isActive;
    console.log(row.isActive)
    // this.showAdd = false;
    // this.showUpdate = true;
    this.groupObj.groupId = row.id;
    // this.rolesList.controls['roleId'].setValue(row.id);
    this.groupList.controls['groupName'].setValue(row.groupName);
    this.groupList.controls['grouptitle'].setValue(row.grouptitle);
    this.groupList.controls['grouptitleAr'].setValue(row.grouptitleAr);
    this.groupList.controls['groupActive'].setValue(row.groupActive);
    this.updateGroupDetails();
  }

  onEdit(row:any){
    this.showAdd = false;
    this.showUpdate = true;
    this.groupObj.groupId = row.id;
    // this.rolesList.controls['roleId'].setValue(row.id);
    this.groupList.controls['groupName'].setValue(row.groupName);
    this.groupList.controls['grouptitle'].setValue(row.grouptitle);
    this.groupList.controls['grouptitleAr'].setValue(row.grouptitleAr);
    // this.groupList.controls['isActive'].setValue(row.isActive);
  }

  updateGroupDetails(){
    // this.rolesObj.roleId = this.rolesList.value.roleId;
    this.groupObj.groupName = this.groupList.value.groupName;
    this.groupObj.grouptitle = this.groupList.value.grouptitle;
    this.groupObj.grouptitleAr = this.groupList.value.grouptitleAr;
    this.groupObj.groupActive = this.groupList.value.groupActive;
    this.api.updateGroup(this.groupObj,this.groupObj.groupId)
      .subscribe(res=>{
        alert('updated');
        let ref = document.getElementById('cancel')
        ref?.click();
        this.groupList.reset();
        this.getAllGroups();
      })
  }


  getAllActions(row:any){
    this.currentGroupId = row.id;
    this.api.getAllActions()
      .subscribe(res=>{
        this.groupActionsData = res;
      })
  }

  addGroupAction(row:any){
    this.groupActionObj.actionId = row.id;
    this.groupActionObj.groupId = this.currentGroupId;
    this.api.addGroupAction(this.groupActionObj)
      .subscribe( res=>{
        alert('GroupAction Added');
      })
  }

  viewGroupActions(){
    this.api.getGroupActions()
      .subscribe(res=>{
        this.groupActionsList = res;
      })
  }

}
