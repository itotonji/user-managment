import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupsComponent } from './groups.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


const routes :Routes=[
  // {path:'user',component:UserComponentComponent},
  {path:'groups',component:GroupsComponent}
]
@NgModule({
  declarations: [
    GroupsComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    ReactiveFormsModule
  ]
})
export class GroupsModule { }
