import { Component, OnInit } from '@angular/core';

import { Facility } from './Facility';
import { FacilityService } from './FacilityService';

@Component({
  selector: 'app-facility-list',
  templateUrl: './facility-list.component.html',
  styleUrls: ['./facility-list.component.css']
})



export class FacilityListComponent implements OnInit {

  facilities: Facility[] = [];

    constructor(private facilityService: FacilityService) { }

    ngOnInit() {
      this.facilities = this.facilityService.getFacilities();  
    }



}
