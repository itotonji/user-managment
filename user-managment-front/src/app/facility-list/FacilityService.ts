import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';

import { Facility } from './Facility';
export interface Config {
    data: any;
  }

@Injectable()
export class FacilityService {


    constructor(private http: HttpClient) {}

    getFacilities() {
        return [{"name": "HIS", "license": '2012'}];
        
    }
}
