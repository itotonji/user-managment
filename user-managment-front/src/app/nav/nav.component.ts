import { Component, OnInit } from '@angular/core';
import {Emitters} from "../service/Emitters";
import {AuthServices} from "../service/auth.services";
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private  auth:AuthServices ,private route:Router) { }
//@ts-ignore
  authvalue =false;
  ngOnInit(): void {
    Emitters.authEmitter.subscribe(
      (auth:boolean)=> {this.authvalue=auth;}
    )
   }
  // logOut()
  // {
  //   this.auth.logOut().subscribe(res=>{
  //     alert('log out')
  //     localStorage.removeItem('token')
  //
  //     Emitters.authEmitter.emit(false)
  //     this.auth.userToken.next('')
  //     this.route.navigate(['/'])
  //
  //   })
  // }
}
