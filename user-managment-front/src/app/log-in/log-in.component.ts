import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AuthServices} from "../service/auth.services";
import {HttpClient} from "@angular/common/http";
import  {admin} from "../service/admin"
import {user} from "../user/user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css'],
})
export class LogInComponent implements OnInit {
 profileForm !:FormGroup;
 admin !:admin;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthServices,)
  { }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      username: [''],
      password: ['']
    });
    this.admin= new admin();
  }
 //  getUser(){
 //    this.admin.username=<string>this.profileForm.value.username
 //    this.admin.password=<string>this.profileForm.value.password
 //    return this.admin
 //  }
 //  logIn() {
 //    this.authService.login(this.getUser())
 //  }
}
