import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import { RolesComponent } from './roles.component';
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "../user/user-component/userComponent";


const routes :Routes=[
  // {path:'user',component:UserComponentComponent},
  {path:'roles',component:RolesComponent}
]

@NgModule({
  declarations: [
    RolesComponent

  ],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule,
    ReactiveFormsModule

  ],
  providers:[ReactiveFormsModule]
})
export class RolesModule { }
