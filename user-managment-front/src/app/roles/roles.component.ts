import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {roles} from "./role";
import {RoleUser} from "./roleUser";
import {HttpClient} from "@angular/common/http";
import {ApiService} from "../shared/api.service";
import {RoleActions} from "./roleActions";


@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  rolesList!:FormGroup;
  rolesObj: roles = new roles();
  roleUserObj: RoleUser = new RoleUser();
  roleActionObj: RoleActions = new RoleActions();
  rolesData!: any;
  rolesUsersData!: any;
  actionsListData!: any;
  currentRoleId!:any;
  usersData!: any;
  roleUserData!: any;
  showAdd !: boolean;
  showUpdate !: boolean;


  constructor(private fb:FormBuilder,
              private http:HttpClient,
              private api:ApiService
              ) { }

  ngOnInit(): void {
    this.rolesList=this.fb.group({
      'roleName':[''],
      'roleTitle':[''],
      'roleTitleAr':[''],
      'isActive':[true]
    })

    this.getAllRoles();

  }

  postUserDetails(){
    this.rolesObj.roleId = this.rolesList.value.roleId;
    this.rolesObj.name = this.rolesList.value.roleName;
    this.rolesObj.title = this.rolesList.value.roleTitle;
    this.rolesObj.titleAr = this.rolesList.value.roleTitleAr;
    this.rolesObj.isActive = false;

    this.api.postRole(this.rolesObj)
      .subscribe(res=>{
          console.log(res);
          alert('user added sucessfully')
          let ref = document.getElementById('cancel')
          ref?.click();
          this.rolesList.reset();
          this.getAllRoles();
        },
        err=>{
          alert(err)
        })
  }

  toggleActivation(row:any){
    row.isActive = !row.isActive;
    console.log(row.isActive)
    this.rolesObj.roleId = row.id;
    this.rolesList.controls['roleName'].setValue(row.name);
    this.rolesList.controls['roleTitle'].setValue(row.title);
    this.rolesList.controls['roleTitleAr'].setValue(row.titleAr);
    this.rolesList.controls['isActive'].setValue(row.isActive);
    this.updateUserDetails();
  }

  getAllRoles(){
    this.api.getRole()
      .subscribe(res=>{
        this.rolesData = res;
      })
  }

  getAllRoleUsers(){
    // this.currentRoleId = row.id;
    this.api.getRoleUsers()
      .subscribe(res=>{
        this.rolesUsersData = res;
      })
  }

  getAllActions(row:any){
    this.currentRoleId = row.id;
    this.api.getAllActions()
      .subscribe(res=>{
        this.actionsListData = res;
      })
  }

  getAllUsers(row:any){
    this.currentRoleId = row.roleId;
    this.api.getAllUsers()
      .subscribe(res=>{
        this.usersData = res;
      })
  }
  getUsers(){
    this.api.getAllUsers()
      .subscribe(res=>{
        this.usersData = res;
      })
  }

  addRoleUser(row:any){
    this.roleUserObj.userId = row.id;
    this.roleUserObj.roleId = this.currentRoleId;
    this.api.addRoleUser(this.roleUserObj)
      .subscribe(res=>{
        alert('RoleUser Added');
      })
    this.getUsers();
  }

  addAction(row:any){
    this.roleActionObj.actionId = row.id;
    this.roleActionObj.roleId = this.currentRoleId;
    this.api.addRoleAction(this.roleActionObj)
      .subscribe(res=>{
        alert('RoleAction Added');
      })
    this.getUsers();
  }
  removeRoleUser(row:any){
    this.api.removeRoleUser(row.id)
      .subscribe(res=>{
        alert('RoleUser deleted');
      })
    this.getAllRoleUsers();
  }

  clickAddRole(){
    this.rolesList.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }

  deleteUser(row:any){
    this.api.deleteRole(row.id)
      .subscribe(res=>{
        alert('user deleted');
        this.getAllRoles();
      })
  }

  onEdit(row:any){
    this.showAdd = false;
    this.showUpdate = true;
    this.rolesObj.roleId = row.id;
    this.rolesList.controls['roleName'].setValue(row.name);
    this.rolesList.controls['roleTitle'].setValue(row.title);
    this.rolesList.controls['roleTitleAr'].setValue(row.titleAr);
  }

  updateUserDetails(){
    this.rolesObj.name = this.rolesList.value.roleName;
    this.rolesObj.title = this.rolesList.value.roleTitle;
    this.rolesObj.titleAr = this.rolesList.value.roleTitleAr;
    this.rolesObj.isActive = this.rolesList.value.isActive;
    this.api.updateRole(this.rolesObj,this.rolesObj.roleId)
      .subscribe(res=>{
        alert('updated');
        let ref = document.getElementById('cancel')
        ref?.click();
        this.rolesList.reset();
        this.getAllRoles();
      })
  }


}
