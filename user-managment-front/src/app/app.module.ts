import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import {TableModule} from 'primeng/table';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FacilityService} from "./facility-list/FacilityService";
// import {FacilityListComponent} from "./facility-list/facility-list.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserModule} from "./user/user.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FacilitiesModule} from "./facilities/facilities.module";
import {RolesModule} from "./roles/roles.module";
import {GroupsModule} from "./groups/groups.module"
import {IntroModule} from './intro/intro.module';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  // {path: 'facility', component: FacilityListComponent}

];


@NgModule({
  declarations: [
    AppComponent,
    // FacilityListComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    TableModule,
    RouterModule.forRoot(routes),
    UserModule,
    FacilitiesModule,
    RolesModule,
    GroupsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    IntroModule
  ],
  exports: [
    RouterModule
  ],
  providers: [FacilityService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
