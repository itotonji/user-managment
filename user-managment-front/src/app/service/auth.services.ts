import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject, map, observable, Observable, Subscription, tap, throwError} from "rxjs";
import {Injectable} from "@angular/core";

import {Router} from "@angular/router";
import {Emitters} from "./Emitters";
@Injectable({
  providedIn: 'root',
})
export class AuthServices {
   url !:string;
  userToken !:any
  isauth!:boolean
  msg!:string
  constructor(private http: HttpClient, private router: Router) {


     this.userToken = new BehaviorSubject<string>('')
     this. isauth = false;
    this.msg = "your not logged in"
    this.url='localhost:8080/api/login'
  }
  login(user: { username: string, password: string }) {
    return this.http.post<any>(this.url, user).subscribe(
      res => {
        this.isauth = true;
       // localStorage.setItem('token', res.data.auth.token)
        this.userToken.next('res.data.auth.token')
        Emitters.authEmitter.emit(true)

        //  this.router.navigate(['/profile'])
      }, error => {
        this.msg = "you are not logged",
          console.log(alert(this.msg)),
          this.router.navigate(['/login'])
        Emitters.authEmitter.emit(false)
      },
    )
  }

  logOut() {
    return this.http.post<any>(this.url + 'logout', null,)
  }

  getAuthStatus() {
    // this.userToken.subscribe(d=>{
    //   console.log('subscribe to token')
    //
    // })
    if (this.userToken.getValue() != null) {
      console.log('OBS token valid')
      return true;
    }
    return false;
  }

  get getJwtToken() {
    return this.userToken.getValue()
  }

}
