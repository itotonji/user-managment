import { Injectable,Injector } from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest, HttpEvent} from "@angular/common/http";
import { Observable } from 'rxjs';
import {AuthServices } from "./auth.services"


@Injectable()
export class TokenInterceptorService implements HttpInterceptor{

  constructor( private injector:Injector) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
    let authService = this.injector.get(AuthServices )
    if(authService.getJwtToken){
      let tokenReq = req.clone({
        setHeaders:{
          Authorization: `Bearer ${authService.getJwtToken}`
        }
      })
      return next.handle(tokenReq)
    }else {
      return next.handle(req)
    }
  }
}
