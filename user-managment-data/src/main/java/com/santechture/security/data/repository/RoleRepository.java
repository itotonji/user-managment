package com.santechture.security.data.repository;

import com.santechture.security.data.model.Role;
import com.santechture.security.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Integer> {
}
