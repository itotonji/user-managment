package com.santechture.security.data.services;

import com.santechture.security.data.exceptions.ResourceNotFoundException;
import com.santechture.security.data.model.Facility;
import com.santechture.security.data.repository.FacilityRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
@Slf4j
public class FacilityService {

    private final FacilityRepository facilityRepository;

    public Facility createFacility(Facility facility){
        return facilityRepository.saveAndFlush(facility);
    }

    public Facility updateFacility(Facility facility){
        return facilityRepository.saveAndFlush(facility);
    }

    public String deleteFacilityById(Integer id){
        facilityRepository.deleteById(id);
        log.info("facility with id {} deleted successfully",id);
        return "deleted successfully";
    }

    public List<Facility> getAllFacilities(){
        return facilityRepository.findAll();
    }

    @SneakyThrows
    public Facility getFacilityById(Integer id) {
        return facilityRepository.findById(id)
                .orElseThrow(()
                        -> new ResourceNotFoundException("Facility with id : " + id + "not found"));
    }

}
