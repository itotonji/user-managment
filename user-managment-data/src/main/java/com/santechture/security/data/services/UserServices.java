package com.santechture.security.data.services;

import com.santechture.security.data.exceptions.InvalidPasswordException;
import com.santechture.security.data.exceptions.ResourceNotFoundException;
import com.santechture.security.data.model.User;
import com.santechture.security.data.repository.UserRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServices {
    private final UserRepository userRepository;
    @Autowired
    public UserServices(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User addUser(User user){
        return userRepository.save(user);
    }

    public User editUser(User user){

        return userRepository.save(user);
    }

    public String deleteUserById(Integer id){
        userRepository.deleteById(id);
        return "User Deleted";
    }

    public User userActivation(User user){
        return userRepository.saveAndFlush(user);
    }

    @SneakyThrows
    public User resetPassword(Integer userId, String oldPassword, String newPassword){
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User with id : " + userId + "not found"));

        if (user.getPassword().equals(oldPassword)) {
            user.setPassword(newPassword);
            return userRepository.saveAndFlush(user);
        }
        else {
            throw new InvalidPasswordException("password isn't correct");
        }


    }


}
