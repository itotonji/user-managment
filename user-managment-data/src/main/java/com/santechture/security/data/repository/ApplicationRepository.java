package com.santechture.security.data.repository;
import com.santechture.security.data.model.Action;
import com.santechture.security.data.model.Application;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ApplicationRepository extends JpaRepository<Application,Integer> {
   
    
}
