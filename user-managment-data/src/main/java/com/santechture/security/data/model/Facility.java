package com.santechture.security.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "FACILITY")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Facility {
    private String facilityName;
    private String facilityLicense;
    private String facilityLocation;
    private Integer facilityTypeId;
    private Integer facilityStatusId;
    private Date startDate;
    private Date endDate;
    private Integer facilityId;
    private Integer cashPatientPriceListId;
    private boolean installed;
    private Boolean isActive;
    private int departments;
    private String officeStartTime;
    private String officeEndTime;
    private Integer slotsPerHour;
    private int regulator;
    private Integer batchClaimsMaxCount;
    private Integer batchingPeriod;
    private String abbreviation;
    private String pharmacyFacilityLicense;
    private Integer groupId;
    private Date creationDate;
    private Integer createdBy;
    private Date modifyDate;
    private Integer modifyBy;
    private boolean isDeleted;
    private Integer pharmacyFacilityId;
    private String facilityCode;
    private Integer accumedBankAccountId;
    private String arabicFacilityName;
    private String preAuthManagment;
    private String loginLogo;
    private String headerLogo;

    @Basic
    @Column(name = "FACILITY_NAME")
    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    @Basic
    @Column(name = "FACILITY_LICENSE")
    public String getFacilityLicense() {
        return facilityLicense;
    }

    public void setFacilityLicense(String facilityLicense) {
        this.facilityLicense = facilityLicense;
    }

    @Basic
    @Column(name = "FACILITY_LOCATION")
    public String getFacilityLocation() {
        return facilityLocation;
    }

    public void setFacilityLocation(String facilityLocation) {
        this.facilityLocation = facilityLocation;
    }

    @Basic
    @Column(name = "FACILITY_TYPE_ID")
    public Integer getFacilityTypeId() {
        return facilityTypeId;
    }

    public void setFacilityTypeId(Integer facilityTypeId) {
        this.facilityTypeId = facilityTypeId;
    }

    @Basic
    @Column(name = "FACILITY_STATUS_ID")
    public Integer getFacilityStatusId() {
        return facilityStatusId;
    }

    public void setFacilityStatusId(Integer facilityStatusId) {
        this.facilityStatusId = facilityStatusId;
    }

    @Basic
    @Column(name = "START_DATE")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "END_DATE")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Id
    @Column(name = "FACILITY_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
    }

    @Basic
    @Column(name = "CASH_PATIENT_PRICE_LIST_ID")
    public Integer getCashPatientPriceListId() {
        return cashPatientPriceListId;
    }

    public void setCashPatientPriceListId(Integer cashPatientPriceListId) {
        this.cashPatientPriceListId = cashPatientPriceListId;
    }

    @Basic
    @Column(name = "installed")
    public boolean isInstalled() {
        return installed;
    }

    public void setInstalled(boolean installed) {
        this.installed = installed;
    }

    @Basic
    @Column(name = "IS_ACTIVE")
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Basic
    @Column(name = "DEPARTMENTS")
    public int getDepartments() {
        return departments;
    }

    public void setDepartments(int departments) {
        this.departments = departments;
    }

    @Basic
    @Column(name = "OFFICE_START_TIME")
    public String getOfficeStartTime() {
        return officeStartTime;
    }

    public void setOfficeStartTime(String officeStartTime) {
        this.officeStartTime = officeStartTime;
    }

    @Basic
    @Column(name = "OFFICE_END_TIME")
    public String getOfficeEndTime() {
        return officeEndTime;
    }

    public void setOfficeEndTime(String officeEndTime) {
        this.officeEndTime = officeEndTime;
    }

    @Basic
    @Column(name = "SLOTS_PER_HOUR")
    public Integer getSlotsPerHour() {
        return slotsPerHour;
    }

    public void setSlotsPerHour(Integer slotsPerHour) {
        this.slotsPerHour = slotsPerHour;
    }

    @Basic
    @Column(name = "REGULATOR")
    public int getRegulator() {
        return regulator;
    }

    public void setRegulator(int regulator) {
        this.regulator = regulator;
    }

    @Basic
    @Column(name = "BATCH_CLAIMS_MAX_COUNT")
    public Integer getBatchClaimsMaxCount() {
        return batchClaimsMaxCount;
    }

    public void setBatchClaimsMaxCount(Integer batchClaimsMaxCount) {
        this.batchClaimsMaxCount = batchClaimsMaxCount;
    }

    @Basic
    @Column(name = "BATCHING_PERIOD")
    public Integer getBatchingPeriod() {
        return batchingPeriod;
    }

    public void setBatchingPeriod(Integer batchingPeriod) {
        this.batchingPeriod = batchingPeriod;
    }

    @Basic
    @Column(name = "ABBREVIATION")
    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Basic
    @Column(name = "PHARMACY_FACILITY_LICENSE")
    public String getPharmacyFacilityLicense() {
        return pharmacyFacilityLicense;
    }

    public void setPharmacyFacilityLicense(String pharmacyFacilityLicense) {
        this.pharmacyFacilityLicense = pharmacyFacilityLicense;
    }

    @Basic
    @Column(name = "GROUP_ID")
    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Basic
    @Column(name = "created_by")
    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "modify_date")
    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Basic
    @Column(name = "modify_by")
    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    @Basic
    @Column(name = "PHARMACY_FACILITY_ID")
    public Integer getPharmacyFacilityId() {
        return pharmacyFacilityId;
    }

    public void setPharmacyFacilityId(Integer pharmacyFacilityId) {
        this.pharmacyFacilityId = pharmacyFacilityId;
    }

    @Basic
    @Column(name = "facility_code")
    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    @Basic
    @Column(name = "ACCUMED_BANK_ACCOUNT_ID")
    public Integer getAccumedBankAccountId() {
        return accumedBankAccountId;
    }

    public void setAccumedBankAccountId(Integer accumedBankAccountId) {
        this.accumedBankAccountId = accumedBankAccountId;
    }

    @Basic
    @Column(name = "arabic_facility_name")
    public String getArabicFacilityName() {
        return arabicFacilityName;
    }

    public void setArabicFacilityName(String arabicFacilityName) {
        this.arabicFacilityName = arabicFacilityName;
    }

    @Basic
    @Column(name = "PRE_AUTH_MANAGMENT")
    public String getPreAuthManagment() {
        return preAuthManagment;
    }

    public void setPreAuthManagment(String preAuthManagment) {
        this.preAuthManagment = preAuthManagment;
    }

    @Basic
    @Column(name = "LOGIN_LOGO")
    public String getLoginLogo() {
        return loginLogo;
    }

    public void setLoginLogo(String loginLogo) {
        this.loginLogo = loginLogo;
    }

    @Basic
    @Column(name = "HEADER_LOGO")
    public String getHeaderLogo() {
        return headerLogo;
    }

    public void setHeaderLogo(String headerLogo) {
        this.headerLogo = headerLogo;
    }

}
