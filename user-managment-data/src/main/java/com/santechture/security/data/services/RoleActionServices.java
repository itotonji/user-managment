package com.santechture.security.data.services;
import com.santechture.security.data.model.Role;
import com.santechture.security.data.model.Action;
import com.santechture.security.data.model.RoleAction;
import com.santechture.security.data.repository.RoleActionRepository;
import com.santechture.security.data.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleActionServices
{
    private final RoleActionRepository roleActionRepository;
    @Autowired
    public RoleActionServices(RoleActionRepository roleActionRepository) {
        this.roleActionRepository = roleActionRepository;
    }

    public List<RoleAction> getallroleaction(){
        return roleActionRepository.findAll();
    }
    public RoleAction addRoleAction(Role role, Action action){
        RoleAction roleAction = RoleAction.builder()
                .role(role).action(action).build();
        return roleActionRepository.save(roleAction);
    }
    public String deleteRoleAction(Integer id){
        roleActionRepository.deleteById(id);
        return "Role Action Deleted";
    }

}
