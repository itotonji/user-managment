package com.santechture.security.data.repository;

import com.santechture.security.data.model.Action;
import com.santechture.security.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ActionRepository extends JpaRepository<Action,Integer> {
     @Query("SELECT u FROM Action u WHERE u.id = ?1")
    Action findActionByid(Integer status);
     @Query("SELECT u FROM Action u WHERE u.module.id = ?1")
    Action findActionByModuleId(Integer status);
}
