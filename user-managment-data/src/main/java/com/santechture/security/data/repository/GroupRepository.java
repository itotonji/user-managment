package com.santechture.security.data.repository;

import com.santechture.security.data.model.Group;
import com.santechture.security.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<Group,Integer> {
}
