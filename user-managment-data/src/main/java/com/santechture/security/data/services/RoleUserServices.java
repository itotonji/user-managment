package com.santechture.security.data.services;
import com.santechture.security.data.model.RoleUser;
import com.santechture.security.data.model.User;
import com.santechture.security.data.model.Role;
import com.santechture.security.data.repository.RoleUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class RoleUserServices
{
    private final RoleUserRepository roleUserRepository;
    @Autowired
    public RoleUserServices(RoleUserRepository roleUserRepository) {
        this.roleUserRepository = roleUserRepository;
    }

    public List<RoleUser> getAllRoleUsers(){
        return roleUserRepository.findAll();
    }
    public RoleUser addRoleUser(Role role,User user){
        RoleUser roleUser = RoleUser.builder().role(role).user(user).build();
        return roleUserRepository.save(roleUser);
    }


    public String deleteRoleUser(Integer id){
        roleUserRepository.deleteById(id);
        return "Role User Deleted";
    }
}
