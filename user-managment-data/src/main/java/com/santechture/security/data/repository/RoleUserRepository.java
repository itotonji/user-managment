package com.santechture.security.data.repository;

import com.santechture.security.data.model.RoleUser;
import com.santechture.security.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleUserRepository extends JpaRepository<RoleUser,Integer> {
}
