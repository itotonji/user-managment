package com.santechture.security.data.services;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.santechture.security.data.model.FacilityUser;
import com.santechture.security.data.repository.FacilityUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class FacilityUserServices {
    @Autowired
    private final FacilityUserRepository facilityUserRepository ;
    
        
    @Autowired
  
    public FacilityUserServices(FacilityUserRepository facilityUserRepository) {
        this.facilityUserRepository = facilityUserRepository;
    }
    
    public List<FacilityUser> getAllfacilityUsers(){
        return facilityUserRepository.findAll();
    }
    

     public FacilityUser  AddFacilityUser( FacilityUser facilityUser) {
        FacilityUser newfacilityUser = facilityUserRepository
                .save(FacilityUser.builder()
                        .facility(facilityUser.getFacility())
                        .user(facilityUser.getUser())
                        .build());
        return facilityUserRepository.save(facilityUser);

}
    
       public String deletefacilityUser(Integer id){
        facilityUserRepository.deleteById(id);
        return "facility User Deleted";
    }
    
    
}
