package com.santechture.security.data.services;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.santechture.security.data.model.Action;
import com.santechture.security.data.repository.ActionRepository;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 *
 * @author Leen
 */
@Service
public class ActionsServices {
    ActionRepository actionRepository;
    
    public List<Action> getAllActions(){
        return actionRepository.findAll();
    }
      public Action  AddAction( Action action) {
        Action newAction = actionRepository
                .save(Action.builder()
                        .code(action.getCode())
                        .name(action.getName())
                        .title(action.getTitle())
                        .titleAr(action.getTitleAr())
                        .url(action.getUrl())
                        .isActive(action.isActive())
                        .isview(action.isIsview())
                        .group(action.getGroup())
                        .module(action.getModule())
                        .build());
        return actionRepository.save(action);

}
      public Action  editAction( Action action){
         
           Action actionobj = (Action) actionRepository.findActionByid(action.getId());

        if (actionobj != null) {
            actionobj.setName(action.getName());
            actionobj.setCode(action.getCode());
            actionobj.setTitle(action.getTitle());
            actionobj.setTitleAr(action.getTitleAr());
            actionobj.setUrl(action.getUrl());
            actionobj.setActive(action.isActive());
            actionobj.setIsview(action.isIsview());
            actionobj.setGroup(action.getGroup());
            actionobj.setModule(action.getModule());
            return actionRepository.save(action);
           
        }

        return null;
      }
      public  Action getActionById(Integer id) {
       
        return actionRepository.findActionByid(id);


      }
      
}
