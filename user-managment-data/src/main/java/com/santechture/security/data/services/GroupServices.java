package com.santechture.security.data.services;

import com.santechture.security.data.model.Group;
import com.santechture.security.data.repository.ActionRepository;
import com.santechture.security.data.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServices {
private final GroupRepository groupRepository;
@Autowired
public GroupServices(GroupRepository groupRepository) {
    this.groupRepository = groupRepository;
}

    

    public List<Group> getallGroup(){
        return groupRepository.findAll();
    }


    public Group addGroup(Group group){
//        Group group = Group.builder().code(group.getCode()).name(group.getName()).title(group.getTitle()).titleAr(group.getTitleAr()).isActive(group.isIsActive()).build();
        return groupRepository.save(group);
    }


    public Group editGroup(Group group){
        //Group group = Group.builder().id(id).code(code).name(name).title(title).titleAr(titleAr).isActive(isActive).build();
        return groupRepository.save(group);
    }

    public String deleteGroup(Integer id){
        groupRepository.deleteById(id);
        return "Group Deleted";
    }






}







