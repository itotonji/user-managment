package com.santechture.security.data.repository;

import com.santechture.security.data.model.Module;
import com.santechture.security.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModuleRepository extends JpaRepository<Module,Integer> {
}
