package com.santechture.security.data.repository;

import com.santechture.security.data.model.RoleAction;
import com.santechture.security.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleActionRepository extends JpaRepository<RoleAction,Integer> {
}
