package com.santechture.security.data.repository;

import com.santechture.security.data.model.Facility;
import com.santechture.security.data.model.FacilityUser;
import com.santechture.security.data.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FacilityUserRepository extends JpaRepository<FacilityUser,Integer> {
   
    
}
