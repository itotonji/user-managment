package com.santechture.security.data.services;

import com.santechture.security.data.model.Role;
import com.santechture.security.data.model.User;
import com.santechture.security.data.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServices
{
    private final RoleRepository roleRepository;
    @Autowired
    public RoleServices(RoleRepository rolerepository) {
        this.roleRepository = rolerepository;
    }

    public List<Role> getAllRoles(){

        return roleRepository.findAll();
    }

    public Role addRole(String name, String title, String titlear, boolean isActive){
        Role role = Role.builder().name(name).title(title).titleAr(titlear).isActive(isActive).build();
        return roleRepository.save(role);
    }
    
     public Role addRole(Role role){
        return roleRepository.save(role);
    }

    public Role editRole(int id,String name, String title, String titlear, boolean isActive){
        Role role = Role.builder().id(id).name(name).title(title).titleAr(titlear).isActive(isActive).build();
        return roleRepository.save(role);
    }
    
    public Role editRole(Role role){
        return roleRepository.save(role);
    }

}
