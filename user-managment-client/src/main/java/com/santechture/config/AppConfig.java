package com.santechture.config;

import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.orm.hibernate5.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
@Configuration
@ComponentScan(basePackages="com.santechture.security.data.services")
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.santechture.security.data.repository" })
public class AppConfig 
{
        
	@Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer()
    {
        return new PropertySourcesPlaceholderConfigurer();
    }
    
    @Bean
    public PlatformTransactionManager transactionManager(DataSource dataSource)
    {
        EntityManagerFactory factory = entityManagerFactory(dataSource).getObject();
        return new JpaTransactionManager(factory);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource)
    {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(Boolean.FALSE);
        vendorAdapter.setShowSql(Boolean.FALSE);

        factory.setDataSource(dataSource);
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.santechture");

        Properties jpaProperties = new Properties();
        jpaProperties.put("spring.jpa.hibernate.ddl-auto", "update");
        jpaProperties.put("spring.jpa.properties.hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
        factory.setJpaProperties(jpaProperties);

        factory.afterPropertiesSet();
        factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
        return factory;
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator()
    {
        return new HibernateExceptionTranslator();
    }
    
    @Bean
    public DataSourceInitializer dataSourceInitializer(DataSource dataSource) 
    {
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
//        String dbInitializationEnabled = env.resolvePlaceholders("${jdbc.init-db}");
//        dataSourceInitializer.setEnabled(Boolean.parseBoolean(dbInitializationEnabled));
        dataSourceInitializer.setDataSource(dataSource);
//        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
//        String dbInitScriptLocation = env.resolvePlaceholders("${jdbc.initLocation}");
//        if(StringUtils.isNotEmpty(dbInitScriptLocation))
//        {
//            databasePopulator.setScripts(new Resource[]{
//                    new ClassPathResource(dbInitScriptLocation)
//            });
//        }
//        dataSourceInitializer.setDatabasePopulator(databasePopulator);
        return dataSourceInitializer;
    }
    
   	@Bean
   	public DataSource dataSource()
   	{
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName("org.hibernate.dialect.SQLServer2012Dialect");
      dataSource.setUrl("jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=SecurityDB;encrypt=true;trustServerCertificate=true;");
      dataSource.setUsername("admin123");
      dataSource.setPassword("admin123");
      return dataSource;
        }


   
    
   
}
