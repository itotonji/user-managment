package com.santechture.bean;
import com.santechture.security.data.model.Role;
import com.santechture.security.data.model.User;
import com.santechture.security.data.services.RoleServices;
import com.santechture.security.data.services.UserServices;
import org.primefaces.PrimeFaces;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import javax.faces.context.FacesContext;

import org.springframework.stereotype.Service;

@ManagedBean(name = "userBean")
@SessionScoped
@Service
public class UsersBean implements Serializable {
    boolean isUpdated=false;
    private User usertemp=new User();

    private List<User> users;
    private User user=new User();

    private User selectedUser;

    private List<User> selectedUsers;

    @ManagedProperty(value = "#{userServices}")
    private UserServices userServices;

    @PostConstruct
    public void init() {
      this.users=getUserServices().getAllUsers();
    }

    public boolean isIsUpdated() {
        return isUpdated;
    }

    public void setIsUpdated(boolean isUpdated) {
        this.isUpdated = isUpdated;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUsertemp() {
        return usertemp;
    }

    public void setUsertemp(User usertemp) {
        this.usertemp = usertemp;
    }
    

    public List<User> getUsers() {
        return users;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<User> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(List<User> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public void openNew() {
        this.selectedUser = new User();
    }

    public void saveUser() {
        if (this.selectedUser.getId() == 0) {
            this.users.add(this.selectedUser);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User Added"));
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User Updated"));
        }

        PrimeFaces.current().executeScript("PF('manageUserDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-Users");
    }




    public boolean hasSelectedUsers() {
        return this.selectedUsers != null && !this.selectedUsers.isEmpty();
    }

    /**
     * @return the userServices
     */
    public UserServices getUserServices() {
        return userServices;
    }

    /**
     * @param userServices the userServices to set
     */
    public void setUserServices(UserServices userServices) {
        this.userServices = userServices;
    }
    public void doadd()
    {
        
        userServices.addUser(user);
        users.add(user);
        user=new User();
        
        
        

    }
     public void doedit(){
        userServices.editUser(user);

    }
    

}

