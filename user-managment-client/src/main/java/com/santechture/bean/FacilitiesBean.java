/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santechture.bean;


import com.santechture.security.data.model.Facility;
import com.santechture.security.data.model.FacilityUser;
import com.santechture.security.data.services.FacilityService;
import com.santechture.security.data.services.FacilityUserServices;
import com.santechture.security.data.model.User;
import com.santechture.security.data.repository.FacilityUserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Leen
 */

@ManagedBean(name="facilitiesBean")
@ViewScoped
public class FacilitiesBean {

List<Facility> facilitylist=new ArrayList();
private List<Facility> selectedfacilities;
private Facility facility =new Facility();
FacilityService facilityService;

FacilityUser facilityUser=new FacilityUser();
List<FacilityUser> listFacilityUser= new ArrayList<>();
List<FacilityUser> selected_FacilityUser= new ArrayList<>();

FacilityUserServices facilityUserServices;
User user = new User ();

FacilityUserRepository FacilityUserRepository;
    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public List<Facility> getFacilitylist() {
        return facilitylist;
    }

    public void setFacilitylist(List<Facility> facilitylist) {
        this.facilitylist = facilitylist;
    }

    public List<Facility> getSelectedfacilities() {
        return selectedfacilities;
    }

    public void setSelectedfacilities(List<Facility> selectedfacilities) {
        this.selectedfacilities = selectedfacilities;
    }
    public void openNew() {
        this.facility = new Facility();
    }  

    public FacilityService getFacilityService() {
        return facilityService;
    }

    public void setFacilityService(FacilityService facilityService) {
        this.facilityService = facilityService;
    }

    public FacilityUser getFacilityUser() {
        return facilityUser;
    }

    public void setFacilityUser(FacilityUser facilityUser) {
        this.facilityUser = facilityUser;
    }

    public List<FacilityUser> getListFacilityUser() {
        return listFacilityUser;
    }

    public void setListFacilityUser(List<FacilityUser> listFacilityUser) {
        this.listFacilityUser = listFacilityUser;
    }

    public List<FacilityUser> getSelected_FacilityUser() {
        return selected_FacilityUser;
    }

    public void setSelected_FacilityUser(List<FacilityUser> selected_FacilityUser) {
        this.selected_FacilityUser = selected_FacilityUser;
    }


    public FacilitiesBean() {
    }
    public void saveFacility() throws Exception  {
        
    try {
        if(this.facility ==null) throw new Exception ("No Facility Information");
       
        if( this.facility.getFacilityId()==null) {
            
            facilityService.createFacility(facility);
            this.facilitylist.add(this.facility);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Facility Added"));
       
           }else{
           facilityService.updateFacility(facility);
            
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Facility Updated"));
        
            }
        PrimeFaces.current().executeScript("PF('FacilityDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-Facilities");
    } catch (Exception ex) {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Save Facility",ex.getMessage()));
        Logger.getLogger(FacilitiesBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    
     
     
   
     FacilityUser newfacilityUser=new FacilityUser();
    
     public void savefacilityUser(){
         try {
        if(this.facility ==null) throw new Exception ("No user Information");
        if (newfacilityUser==null)throw new Exception ("No facility user Information");
        if (newfacilityUser.getUser()==null)throw new Exception ("No user id Information");
        
        newfacilityUser.setUser(user);
        facilityUserServices.AddFacilityUser(newfacilityUser);
        
        this.listFacilityUser.add(this.newfacilityUser);
        PrimeFaces.current().ajax().update("form:messages", "dialogs3:dt-facilityuser");
          
        PrimeFaces.current().executeScript("PF('AdduserDialog').hide()");
        
        }catch (Exception ex) {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Save user",ex.getMessage()));
        Logger.getLogger(FacilitiesBean.class.getName()).log(Level.SEVERE, null, ex);
    } 
     
}
     
      public void fetchFacilityUser(){
        try {
              newfacilityUser = new FacilityUser (); 
//       if(this.facility ==null || this.facility.getFacilityId() ==null ) throw new Exception ("No facility Information");
//           listFacilityUser=FacilityUserRepository.getUserbyfacility(facility);

       } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Facility User",ex.getMessage()));
            Logger.getLogger(FacilitiesBean.class.getName()).log(Level.SEVERE, null, ex);
       } 
    }
     
    
    
    
}
