/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santechture.bean;


import com.santechture.security.data.model.Group;

import com.santechture.security.data.services.GroupServices;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;


@ManagedBean(name="groupBean")
@ViewScoped

public class GroupsBean {

List<Group> grouplist=new ArrayList();
private List<Group> selectedGroups;
private Group group =new Group();
GroupServices groupServices;

    public List<Group> getGrouplist() {
        return grouplist;
    }

    public void setGrouplist(List<Group> grouplist) {
        this.grouplist = grouplist;
    }

    public List<Group> getSelectedGroups() {
        return selectedGroups;
    }

    public void setSelectedGroups(List<Group> selectedGroups) {
        this.selectedGroups = selectedGroups;
    }
    
    public void openNew() {
        this.group = new Group();
    }  

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
    
   
    
   public void saveGroup()  {
        
    try {
        if(this.group ==null) throw new Exception ("No group Information");
       
        if(this.group.getId() ==null) {
            groupServices.addGroup(group);
            this.grouplist.add(this.group);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Group Added"));
       
            }else{
            groupServices.editGroup(group);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Group Updated"));
        
            }
        PrimeFaces.current().executeScript("PF('GroupDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-Groups");
    } catch (Exception ex) {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Save Groups",ex.getMessage()));
        Logger.getLogger(GroupsBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
}
