package com.santechture.bean;
import com.santechture.security.data.model.Role;
import com.santechture.security.data.model.User;
import com.santechture.security.data.model.Action;
import com.santechture.security.data.model.RoleUser;
import com.santechture.security.data.model.RoleAction;
import com.santechture.security.data.services.ActionsServices;
import com.santechture.security.data.services.RoleActionServices;
import com.santechture.security.data.services.RoleServices;
import com.santechture.security.data.services.RoleUserServices;
import com.santechture.security.data.services.UserServices;
import java.io.Serializable;
import org.primefaces.PrimeFaces;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;

@ManagedBean(name = "roleBean")
@SessionScoped
@Service
public class RoleBean implements Serializable
{
    private Role role=new Role();
    private List<Role> roles;
    private RoleUser roleuser=new RoleUser();
    private List<RoleUser> roleusers;
    private User user=new User();
    private List<User> users;
    
   // private Action action=new Action();
    //private List<Action> actions;
  //  private RoleAction roleAction=new RoleAction();
   // private List<RoleAction> roleActions;
    
    private List<RoleUser> temproleuser;
//    @ManagedProperty(value = "#{actionsServices}")
//    private ActionsServices actionsServices;
//    
//    @ManagedProperty(value = "#{roleActionServices}")
//    private RoleActionServices roleActionServices;
    
    @ManagedProperty(value = "#{roleServices}")
    private RoleServices roleServices;
    
    @ManagedProperty(value= "#{roleUserServices}")
    private RoleUserServices roleUserServices;
    
    @ManagedProperty(value= "#{userServices}")
    private UserServices userServices;

    public UserServices getUserServices() {
        return userServices;
    }

    public void setUserServices(UserServices userServices) {
        this.userServices = userServices;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

  

    
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public RoleServices getRoleServices() {
        return roleServices;
    }

    public void setRoleServices(RoleServices roleServices) {
        this.roleServices = roleServices;
    }

    public RoleUser getRoleuser() {
        return roleuser;
    }

    public void setRoleuser(RoleUser roleuser) {
        this.roleuser = roleuser;
    }

    public List<RoleUser> getRoleusers() {
        return roleusers;
    }

    public void setRoleusers(List<RoleUser> roleusers) {
        this.roleusers = roleusers;
    }

    public RoleUserServices getRoleUserServices() {
        return roleUserServices;
    }

    public void setRoleUserServices(RoleUserServices roleUserServices) {
        this.roleUserServices = roleUserServices;
    }

    public void setTemproleuser(List<RoleUser> temproleuser) {
        this.temproleuser = temproleuser;
    }

    public List<RoleUser> getTemproleuser() {
        return temproleuser;
    }

//    public Action getAction() {
//        return action;
//    }
//
//    public void setAction(Action action) {
//        this.action = action;
//    }
//
//    public List<Action> getActions() {
//        return actions;
//    }
//
//    public void setActions(List<Action> actions) {
//        this.actions = actions;
//    }
//
//    public RoleAction getRoleAction() {
//        return roleAction;
//    }

//    public void setRoleAction(RoleAction roleAction) {
//        this.roleAction = roleAction;
//    }
//
//    public List<RoleAction> getRoleActions() {
//        return roleActions;
//    }
//
//    public void setRoleActions(List<RoleAction> roleActions) {
//        this.roleActions = roleActions;
//    }
//
//    public ActionsServices getActionsServices() {
//        return actionsServices;
//    }
//
//    public void setActionsServices(ActionsServices actionsServices) {
//        this.actionsServices = actionsServices;
//    }

   

//    public RoleActionServices getRoleActionServices() {
//        return roleActionServices;
//    }
//
//    public void setRoleActionServices(RoleActionServices roleActionServices) {
//        this.roleActionServices = roleActionServices;
//    }
//    
     

    @PostConstruct
    public void init() {
        this.roles=getRoleServices().getAllRoles();
        this.roleusers=getRoleUserServices().getAllRoleUsers();
        this.users=getUserServices().getAllUsers();
      //  this.actions=getActionsServices().getAllActions();
      //  this.roleActions=getRoleActionServices().getallroleaction();
    }
    
      public void doadd()
    {
        
        roleServices.addRole(role);
        roles.add(role);
        role=new Role();
    }
      public void addroleuser()
      {
          roleUserServices.addRoleUser(role, user);
          roleusers.add(roleuser);
          roleuser=new RoleUser();
      }
//      public void addroleaction()
//      {
//          roleActionServices.addRoleAction(role, action);
//          roleActions.add(roleAction);
//          roleAction=new RoleAction();
//      }
      public void removeroleuser()
      {
          roleUserServices.deleteRoleUser(roleuser.getId());
          roleusers.remove(roleuser);
      }
    
     public void doedit(){
        roleServices.editRole(role);

    }    
     public void doshowusers()
     {
         for(int i=0;i<roleusers.size();i++)
         {
             if(roleusers.get(i).getRole()==role)
             {
                 temproleuser.add(roleusers.get(i));
             }
         }
         roleusers=temproleuser;
     }
    

}
