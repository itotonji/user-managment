package com.santechture.security.api.controller;

import com.santechture.security.api.model.ApiResponse;
import com.santechture.security.api.requestModel.CreateOrUpdateFacilityUserRequestModel;
import com.santechture.security.data.model.FacilityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api")
public class FacilityUserController {

    @Autowired
    com.santechture.security.data.services.FacilityUserServices facilityUserServices;


    public FacilityUserController(com.santechture.security.data.services.FacilityUserServices facilityUserServices) {
        this.facilityUserServices = facilityUserServices;
    }

    @GetMapping("/facilityUser")
    public ResponseEntity<ApiResponse<List<FacilityUser>>> getAllFacilityUser(){
        return ApiResponse.<List<FacilityUser>>builder()
                .data(facilityUserServices.getAllfacilityUsers())
                .code(HttpStatus.FOUND.value())
                .message("All FacilityUser.")
                .build()
                .toResponseEntity();
    }


    @PostMapping("/facilityUser")
    public ResponseEntity<ApiResponse<FacilityUser>> addFacilityUser(@RequestBody CreateOrUpdateFacilityUserRequestModel requestModel){
        System.out.println(requestModel.getFacilityId());
        System.out.println(requestModel.getUserId());
        return ApiResponse.<FacilityUser>builder()
                .data(facilityUserServices.AddFacilityUser(requestModel.toEntity()))
                .code(HttpStatus.CREATED.value())
                .message("FacilityUser Created")
                .build()
                .toResponseEntity();
    }

    @DeleteMapping("/facilityUser/{id}")
    public ResponseEntity<ApiResponse<Boolean>> deleteFacilityUserById(@PathVariable Integer id){
        facilityUserServices.deletefacilityUser(id);

        return ApiResponse.<Boolean>builder()
                .data(true)
                .code(HttpStatus.OK.value())
                .message("facilityUser with id " + id + " deleted successfully")
                .build()
                .toResponseEntity();

    }



}
