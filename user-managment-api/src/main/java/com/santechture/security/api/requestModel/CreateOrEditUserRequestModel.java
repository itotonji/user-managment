package com.santechture.security.api.requestModel;

import com.santechture.security.data.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Slf4j
public class CreateOrEditUserRequestModel {
    private Integer id;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private Boolean isActive;


    public User toEntity(){
        return User.builder()
                .id(id)
                .userName(userName)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .isActive(isActive)
                .build();
    }

}
