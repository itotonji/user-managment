package com.santechture.security.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@SpringBootApplication()
@ComponentScan({"com.santechture.security.api.controller"
        ,"com.santechture.security.api.security"
        ,"com.santechture.security.api.security.jwt"
        , "com.santechture.security.data"})
@EnableJpaRepositories(basePackages = { "com.santechture.security.data.repository" })
@EntityScan("com.santechture.security.data.model")
public class SantechtureSecurityApplication{

    public static void main(String[] args) {
        SpringApplication.run(SantechtureSecurityApplication.class, args);
    }


}
