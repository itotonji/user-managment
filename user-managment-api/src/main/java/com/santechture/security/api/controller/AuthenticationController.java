package com.santechture.security.api.controller;

import com.santechture.security.api.model.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
public class AuthenticationController {
    @PostMapping
    private ResponseEntity<ApiResponse<Object>> login(){

        return ResponseEntity.ok(ApiResponse.builder().data("Test").build());
    }
}
