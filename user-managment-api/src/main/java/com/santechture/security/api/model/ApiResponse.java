package com.santechture.security.api.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.ResponseEntity;

@Data
@Builder
public class ApiResponse<T> {

    public ApiResponse(int code, String message, T data)
    {
        this.code = code;
        this.message = message;
        this.data =data;
    }


    public ResponseEntity<ApiResponse<T>> toResponseEntity(){
        return ResponseEntity.ok(this);
    }

    public int code;
    public String message;
    public T data;
}
