package com.santechture.security.api.controller;

import com.santechture.security.api.model.ApiResponse;
import com.santechture.security.api.requestModel.CreateOrUpdateFacilityRequestModel;
import com.santechture.security.data.model.Facility;
import com.santechture.security.data.repository.FacilityRepository;
import com.santechture.security.data.services.FacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FacilityController {

    @Autowired
    private FacilityRepository facilityRepository;
    @Autowired
    private FacilityService facilityService;



    @GetMapping("/facilities")
    public ApiResponse<List<Facility>> getAllFacility(){
        return ApiResponse.<List<Facility>>builder()
                .data(facilityService.getAllFacilities())
                .code(HttpStatus.FOUND.value())
                .message("List of all facility.")
                .build();
    }

    @PostMapping("/facilities")
    public ApiResponse<Facility> addFacility(@RequestBody CreateOrUpdateFacilityRequestModel requestModel){

        return ApiResponse.<Facility>builder()
                .data(facilityService.createFacility(requestModel.toEntity()))
                .code(HttpStatus.CREATED.value())
                .message("facility created")
                .build();
    }


    @PutMapping("/facilities")
    public ApiResponse<Facility> updateFacility(@RequestBody CreateOrUpdateFacilityRequestModel requestModel){
        return ApiResponse.<Facility>builder()
                .data(facilityService.updateFacility(requestModel.toEntity()))
                .code(HttpStatus.OK.value())
                .message("Facility updated successfully")
                .build();
    }

}

