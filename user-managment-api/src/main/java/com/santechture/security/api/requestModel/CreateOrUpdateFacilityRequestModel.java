package com.santechture.security.api.requestModel;

import com.santechture.security.data.model.Facility;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateOrUpdateFacilityRequestModel {
    private String facilityName;
    private String facilityLicense;
    private Integer facilityId;
    private String arabicFacilityName;

    public Facility toEntity() {
        return Facility.builder()
                .facilityId(facilityId)
                .facilityName(facilityName)
                .facilityLicense(facilityLicense)
                .arabicFacilityName(arabicFacilityName)
                .build();
    }





}
